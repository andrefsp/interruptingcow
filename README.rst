Interrupting Cow
================

MOOOOO!


Overview
--------

Interruptingcow is a generic utility can relatively gracefully interrupt your
Python code when it doesn't execute within a specific number of seconds::

    import time
    from interruptingcow import timeout
    from interruptingcow.wrappers import method_timeout

    try:
        with timeout(5):
            # perform a potentially very slow operation
            pass
    except RuntimeError:
        print "didn't finish within 5 seconds"

    @method_timeout(2)
    def some_function(n):
        "This function will timeout whithin 5 seconds"
        # perfome some slow tasks
        time.sleep(n)

    try:
        some_function(4)
    except RuntimeError:
        print "didn't finish the function"


Installation
------------
::

    $ pip install interruptingcow

Caveats
-------

Interruptingcow uses ``signal(SIGALRM)`` to let the operating system interrupt
program execution. This has the following limitations:

1. Python signal handlers only apply to the main thread, so you cannot use this
   from other threads
2. You must not use this in a program that uses ``SIGALRM`` itself
