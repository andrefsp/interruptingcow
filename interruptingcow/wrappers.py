from interruptingcow import timeout

def method_timeout(seconds, exception=RuntimeError):
    """
    This method its meant to be used as a method decorator to raise an
    exception in case of timeout
    """
    def receiver(fn):
        def executor(*args, **kwargs):
            with timeout(seconds, exception):
                return fn(*args, **kwargs)
        return executor
    return receiver

